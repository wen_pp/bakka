/**
 * 后端服务
 */
package org.beykery.bakka.test;

import akka.actor.ActorRef;
import org.beykery.bakka.Bakka;
import org.beykery.bakka.BakkaRequest;
import org.beykery.bakka.BaseActor;
import org.beykery.bakka.Bootstrap;

/**
 *
 * @author beykery
 */
@Bakka(service = "Backend")
public class Backend extends BaseActor
{

  public Backend(Bootstrap bs)
  {
    super(bs);
  }

  @BakkaRequest
  public HI hi(HI hi)
  {
    System.out.println(hi + " Backend, ......");
    return hi;
  }
}
